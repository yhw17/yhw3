#include<graphics.h>
#include<Windows.h>
#include<conio.h>
#include<stdio.h>
#include <MMSystem.h>
#pragma comment(lib,"winmm.lib")

//函数声明
void init_game();                  //初始化游戏界面
void end_game();                  //结束

void c_white();                    //白棋
void c_black();                    //黑棋

void play();                     //下棋
int  win();                      //判断胜负  1是黑子win，2是白子win
int flag;						//判断胜负  1是黑子win，2是白子win

void playMusic();

//全局变量

MOUSEMSG m;//鼠标
int turn = 1;	//控制下棋   1黑棋下  2白棋下 黑棋先下
HWND h;		//之后用来接受当前绘图窗口句柄

int chess[22][22] ;      //定义棋子数组		0没有棋子  1黑棋  2白棋
int c_w = 15,c_h = 15,c_wh = 25;   //定义行列数，每格像素数
int c_x0 = 25,c_y0 = 25;           //初始棋子位置
int c_x9 = 375,c_y9 = 375;         //最终棋子位置

/*
主函数
*/
void main()
{
    init_game();

	playMusic();
    play();
	
    end_game();
}

/*
初始化游戏界面
*/
void init_game()    
{
    IMAGE img;
    loadimage(&img, "test.jpg");//导入test图片用作棋盘
    int w, h;
    w = img.getwidth();
    h = img.getheight();
    initgraph(w+150,h);//棋盘右边拓展150用于写选手信息
    putimage( 0, 0, &img);//载入图片

    setlinecolor(WHITE);//画线颜色
    setfillcolor(BLACK);
    
    settextstyle(20,0,"楷体");
	
	fillrectangle(w+35, 75, w+100, 105);
	fillrectangle(w+35, 135, w+100, 165);
	outtextxy(400+40,80,"悔棋");
	outtextxy(w+40,140,"存档");

    outtextxy(w+10,5 ,"黑方:玩家一");
    outtextxy(w+10,30 ,"白方:玩家二");
}

/*
播放背景音乐
*/
void playMusic()
{
	mciSendString("play irony.mp3",NULL,0,0);
	//mciSendString("close bkmusic", NULL, 0, NULL);	//close music
	
	//PlaySound("irony.mp3",NULL,SND_FILENAME | SND_ASYNC);	//only .wav
}

/*
将画笔变为白色
*/
void c_white()  
{
    setlinecolor(WHITE);
    setfillcolor(WHITE);
}

/*
将画笔变为黑色
*/
void c_black()  
{
    setlinecolor(BLACK);
    setfillcolor(BLACK);
}

/*
关闭
*/
void end_game()  
{
	memset(chess,0,sizeof(chess));//初始化数组
    closegraph();
}

/*
判断赢的是哪一方，1代表玩家一，2代表玩家二
*/
int win()
{
	 int i,j;
	if(turn==1)
	{
		for(i = 1 ; i <= 15 ; i++)
		{
			for(j = 1 ; j <= 15 ; j++)
				{
					//横着成五子
					if(chess[i][j] == 1 && chess[i+1][j] == 1 && chess[i+2][j] == 1 &&
					   chess[i+3][j] == 1 && chess[i+4][j] == 1)
					{
						return 1;
					}
					//右斜着成五子
					else if(chess[i][j] == 1 && chess[i+1][j+1] == 1 && chess[i+2][j+2] == 1 &&
							chess[i+3][j+3] == 1 && chess[i+4][j+4] == 1)
					{
						return 1;
					}
					//竖着成五子
					else if(chess[i][j+1] == 1 && chess[i][j+2] == 1 && chess[i][j+3] == 1 &&
							chess[i][j] == 1 && chess[i][j+4] == 1)
					{
						return 1;
					} 
					//左斜着成五子
					else if(i >= 3 && chess[i][j] == 1 && chess[i+1][j-1] == 1 && chess[i+2][j-2] == 1 &&
							chess[i+3][j-3] == 1 && chess[i+4][j-4] == 1)
					{
						return 1;
					}
					else if(j >= 3 && chess[i][j] == 1 && chess[i+1][j-1] == 1 && chess[i+2][j-2] == 1 &&
							chess[i+3][j-3] == 1 && chess[i+4][j-4] == 1)
					{
						return 1;
					}
                
				}
		}
	}else if(turn==2)
	{
		for(i = 1 ; i <= 15 ; i++)
		{
			for(j = 1 ; j <= 15 ; j++)
				{
					//横着成五子
					if(chess[i][j] == 2 && chess[i+1][j] == 2 && chess[i+2][j] == 2 &&
					   chess[i+3][j] == 2 && chess[i+4][j] == 2)
					{
						return 2;
					}
					//右斜着成五子
					else if(chess[i][j] == 2 && chess[i+1][j+1] == 2 && chess[i+2][j+2] == 2 &&
							chess[i+3][j+3] == 2 && chess[i+4][j+4] == 2)
					{
						return 2;
					}
					//竖着成五子
					else if(chess[i][j+1] == 2 && chess[i][j+2] == 2 && chess[i][j+3] == 2 &&
							chess[i][j] == 2 && chess[i][j+4] == 2)
					{
						return 2;
					} 
					//左斜着成五子
					else if(i >= 3 && chess[i][j] == 2 && chess[i+1][j-1] == 2 && chess[i+2][j-2] == 2 &&
							chess[i+3][j-3] == 2 && chess[i+4][j-4] == 2)
					{
						return 2;
					}
					else if(j >= 3 && chess[i][j] == 2 && chess[i+1][j-1] == 2 && chess[i+2][j-2] == 2 &&
							chess[i+3][j-3] == 2 && chess[i+4][j-4] == 2)
					{
						return 2;
					}
                
				}
		}
	}
    return 0;
}
/*
下棋
*/
void play()
{
	

	MessageBox(h, "游戏开始  请下棋", "提示信息", MB_OK);//弹出提示框

	while(1)
	{
		int a,b;	//保存棋子坐标到数组
		m = GetMouseMsg();//获取鼠标信息

		switch (m.uMsg)
		{	
		case WM_LBUTTONDOWN://鼠标左键按下
			if(m.x > 435 && m.x < 500 && m.y > 75 && m.y < 105)
			{
				exit(0);
			}

				//黑棋
				if(turn==1)
				{
					if(m.x > c_x0-c_wh/2 && m.x < c_x9+c_wh/2&&
						 m.y > c_y0-c_wh/2 && m.y < c_y9+c_wh/2)  //判断是否在棋盘内25 - 25/2 < x,y < 375 - 25/2
					{
						  if(m.x % c_wh <= c_wh/2)		//纠正棋子位置，使其落在两线交叉点
							  {
								  a = m.x / c_wh;
							  }else{
								  a = m.x / c_wh + 1;
							  }
							 if(m.y % c_wh <= c_wh/2)
							  {
								  b = m.y / c_wh;
							  }else{
								  b = m.y / c_wh + 1;
							  }
								m.x = a*c_wh;
								m.y = b*c_wh;
								
								
								if(chess[a][b] != 0)  //如果此处有棋子则不能下棋
								{
									MessageBox(h, "这里已经有棋子了，不能下！！！", "五子棋", MB_OK);//弹出提示框
									continue;
								}
									
								chess[a][b] = 1;
								c_black();
								fillcircle(m.x,m.y,10);

								flag=win();
								if(flag == 1)
								{
									MessageBox(h, "玩家一获胜！", "五子棋", MB_OK);//弹出提示框
								}else if(flag==2)
								{
									MessageBox(h, "玩家二获胜！", "五子棋", MB_OK);//弹出提示框
								}								
					}
					turn = 2;
				}else if(turn==2)
				{
					if(m.x > c_x0-c_wh/2 && m.x < c_x9+c_wh/2&&
						 m.y > c_y0-c_wh/2 && m.y < c_y9+c_wh/2)  //判断是否在棋盘内25 - 25/2 < x,y < 375 - 25/2
					{
						  if(m.x % c_wh <= c_wh/2)		//纠正棋子位置，使其落在两线交叉点
							  {
								  a = m.x / c_wh;
							  }else{
								  a = m.x / c_wh + 1;
							  }
							 if(m.y % c_wh <= c_wh/2)
							  {
								  b = m.y / c_wh;
							  }else{
								  b = m.y / c_wh + 1;
							  }
								m.x = a*c_wh;
								m.y = b*c_wh;
								
								if(chess[a][b] != 0)  //如果此处有棋子则不能下棋
								{
									MessageBox(h, "这里已经有棋子了，不能下！！！", "五子棋", MB_OK);//弹出提示框
									continue;
								}	
								chess[a][b] = 2;
								c_white();
								fillcircle(m.x,m.y,10);
								
								flag=win();
								if(flag == 1)
								{
									MessageBox(h, "玩家一获胜！", "五子棋", MB_OK);//弹出提示框
								}else if(flag==2)
								{
									MessageBox(h, "玩家二获胜！", "五子棋", MB_OK);//弹出提示框
								}			
					}
					turn = 1;
				}			
		}
	}
}